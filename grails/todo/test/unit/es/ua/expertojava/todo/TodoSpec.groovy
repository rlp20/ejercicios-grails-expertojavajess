package es.ua.expertojava.todo

import grails.test.mixin.TestFor
import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Todo)
class TodoSpec extends Specification {


    @Unroll
    def " fecha para recordar la tarea deba ser siempre anterior a la fecha de realización de la misma"() {
        given:
        def t1 = new Todo()
        t1.date= date
        t1.reminderDate=reminderDate
        when:
        t1.validate()
        then:
        t1?.errors['reminderDate']
        where:
        date             |   reminderDate
        new Date()       |   new Date()
        new Date()       |   new Date() + 1
    }
    def setup() {
    }

    def cleanup() {
    }

    void "test something"() {
    }
}

<!DOCTYPE html>
<html xmlns:font-size="http://www.w3.org/1999/xhtml">
<head>
    <meta name="layout" content="main"><g:set var="entityName" value="${message(code: 'tag.label', default: 'Tag')}" />
    %{--<title><g:message code="default.create.label" args="[entityName]" /></title>--}%
    %{--<title>Eliminar Tag</title>--}%
</head>
<body>
%{--<a href="#delete-tag" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>--}%

%{--<div id="delete-tag" class="content scaffold-create" role="main">--}%
    %{--<h1>¿Estas seguro de eliminar el tag? </h1>--}%
    %{--<g:if test="${flash.message}">--}%
        %{--<div class="message" role="status">${flash.message}</div>--}%
    %{--</g:if>--}%
    %{--<g:hasErrors bean="${tagInstance}">--}%
        %{--<ul class="errors" role="alert">--}%
            %{--<g:eachError bean="${tagInstance}" var="error">--}%
                %{--<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>--}%
            %{--</g:eachError>--}%
        %{--</ul>--}%
    %{--</g:hasErrors>--}%
    <g:form url="[resource:tagInstance, action:'delete']" method="DELETE">
        <fieldset class="buttons">

            <div id="fotoEli" role="banner">
                <a href="#"><asset:image src="retro-exclamacion.png" style="margin: 0;" alt="Grails"/><a font-size="40px" >¿Desea eliminar el tag?</a></font></a></div>

            %{--<fieldset class="form">--}%
                %{--<g:render template="form"/>--}%
            %{--</fieldset>--}%
            <fieldset class="buttons">
                <g:submitButton name="delete" class="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" />
            </fieldset>
        </fieldset>
    </g:form>
</div>
</body>
</html>
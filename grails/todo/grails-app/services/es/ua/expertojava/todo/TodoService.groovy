package es.ua.expertojava.todo

import grails.transaction.Transactional
import groovy.time.TimeCategory


class TodoService {

    def serviceMethod() {

    }

    def getNextTodos(Integer days, params) {
        Date now = new Date(System.currentTimeMillis())
        Date to = now + days
        Todo.findAllByDateBetween(now, to, params)
    }

    def countNextTodos(Integer days) {
        Date now = new Date(System.currentTimeMillis())
        Date to = now + days
        Todo.countByDateBetween(now, to)
    }

    def listByCategory(String name) {
        Category category = new Category(name)
        Todo.findAllByCategory(category)

    }



    def listTodosByCategory(listaCategorias){
        return Todo.executeQuery("select o from Todo as o where o.category.id in (:list) order by o.date",[list: listaCategorias])
    }


    def saveTodo(Todo todoInstance){
        if(todoInstance.done){
            todoInstance.dateDone = new Date()
        }else{
            todoInstance.dateDone = null
        }

        todoInstance.save flush:true
    }

    def lastTodosDone(Integer horas){
        def fechaActual = new Date();
        def fechaActualMenosNHoras
        use( TimeCategory ) {
            fechaActualMenosNHoras = fechaActual - horas.hours
        }
        List lista = Todo.findAllByLastUpdatedBetween(fechaActualMenosNHoras, fechaActual)
        return lista
    }

    def delete(todoInstance){
        def tags = new ArrayList()

        todoInstance.tags.each { tag ->
            tags.add(tag)
        }

        tags.each{tag->
            todoInstance.removeFromTags(tag)
        }
        todoInstance.delete flush:true
    }

}

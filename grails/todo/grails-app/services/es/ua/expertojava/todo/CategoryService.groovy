package es.ua.expertojava.todo

import grails.transaction.Transactional

@Transactional
class CategoryService {

    def listCategories(){
        Category.findAll()
    }

    def delete(Category categoryInstance){

        categoryInstance.todos.each{todo ->
            todo.setCategory(null)
        }
        categoryInstance.delete flush:true
    }


}

package es.ua.expertojava.todo

class Tag {

    String name
    String color

    static hasMany = [todos:Todo]

    static constraints = {
        name(blank:false, nullable:true, unique:true)
        color shared: "rgbColor"


    }

    String toString(){
        name
    }
}

/* Añade aquí la implementación del factorial en un closure */
def factorial = { int numero ->
    int fact = numero
for(i in numero..<1) {
    if (numero == 0 || numero == 1) 
     fact = numero 
     else
     fact = fact *( i - 1) }
     return fact
}


assert factorial(1)==1
assert factorial(2)==1*2
assert factorial(3)==1*2*3
assert factorial(4)==1*2*3*4
assert factorial(5)==1*2*3*4*5
assert factorial(6)==1*2*3*4*5*6
assert factorial(7)==1*2*3*4*5*6*7
assert factorial(8)==1*2*3*4*5*6*7*8
assert factorial(9)==1*2*3*4*5*6*7*8*9
assert factorial(10)==1*2*3*4*5*6*7*8*9*10

/* Añade a partir de aquí el resto de tareas solicitadas en el ejercicios */

def lista = [2,4,6,8]
lista.each { elemento->
println factorial(elemento)
}

assert factorial(lista[0])==2
assert factorial(lista[1])==24
assert factorial(lista[2])==720
assert factorial(lista[3])==40320

/*fechas*/

def ayer = {fecha ->
   
     Date ayer =  new Date().parse("d/M/yyyy H:m:s",(fecha -1).format("d/M/yyyy  H:m:s"))
}

def manyana = {fecha ->
   
     Date manyana =  new Date().parse("d/M/yyyy H:m:s",(fecha +1).format("d/M/yyyy  H:m:s"))
}

assert ayer(new Date().parse("d/M/yyyy H:m:s","28/6/2008 00:30:20")) == new Date().parse("d/M/yyyy H:m:s","27/6/2008 00:30:20")
assert manyana(new Date().parse("d/M/yyyy H:m:s","28/6/2008 00:30:20")) == new Date().parse("d/M/yyyy H:m:s","29/6/2008 00:30:20")


def fecha2 = new Date().parse("d/M/yyyy H:m:s","02/6/2008 00:30:20")
def fecha3 = new Date().parse("d/M/yyyy H:m:s","03/6/2008 00:30:20")
def fecha4 = new Date().parse("d/M/yyyy H:m:s","04/6/2008 00:30:20")
def listaFechas = [fecha2, fecha3, fecha4]


listaFechas.each{   
    println "Hoy es " + it + " ayer fue " + ayer(it) + "mañana sera" + manyana(it)
 
}




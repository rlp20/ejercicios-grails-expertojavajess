class Libro {

    String Nombre
    int Anyo
    String Autor
    String Editorial
    
    
    Libro(String tit, int anyo, String aut) {
        Nombre = tit
        Anyo = anyo
        Autor = aut
    }
    
    
    String getAutor() {
        def autorTitulo = this.Autor.tokenize(",")
        return autorTitulo[1].trim() + " " + autorTitulo[0]
       
    }
    
    void setEditorial(String edit) {
        Editorial = edit
    }
    /* Añade aquí la definición de la clase */
}

/* Crea aquí las tres instancias de libro l1, l2 y l3 */
l1 = new Libro('La colmena', 1951,'Cela Trulock, Camilo Jose')
l2 = new Libro('La galatea', 1585,'de Cervantes Saavedra, Miguel')
l3 = new Libro('La dorotea', 1585,'Lope de Vega y Carpio, Félix Arturo')
l1.setEditorial('Anaya')
l2.setEditorial('Planeta')
l3.setEditorial('Santillana')




assert l1.getNombre() == 'La colmena'
assert l2.getAnyo() == 1585
assert l3.getAutor() == 'Félix Arturo Lope de Vega y Carpio'

/* Añade aquí la asignación de la editorial a todos los libros */

assert l1.getEditorial() == 'Anaya'
assert l2.getEditorial() == 'Planeta'
assert l3.getEditorial() == 'Santillana'